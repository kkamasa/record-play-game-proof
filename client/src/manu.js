(() => {
  const recordsContainer =  document.getElementById("records");

  function loadRecords() {
    return fetch(`${window.apiUrl}/records`)
      .then((res) => res.json())
      .then((records) => {
        recordsContainer.innerHTML = 'Records: <br>';
        return records.map((r) => {
          const e = document.createElement('div');
          e.innerHTML = `<button><a href="game.html?record=${r.id}">${r.id}</a></button>`;
          recordsContainer.appendChild(e);
        });
      })
  }

  loadRecords();
})();
