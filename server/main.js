const WebSocket = require('ws');
const {createWriteStream} = require('fs');
const fs = require('fs').promises;
const express = require('express');
const cors = require('cors')

const wss = new WebSocket.Server({ port: 8080 });
const api = express();

api.use(cors());

wss.on('connection', function connection(ws) {
  const logStream = createWriteStream(`./records/${Date.now()}.csv`, {flags: 'a'});
  const timeout = setTimeout(() => {
    ws.close()
  }, 1000 * 60 * 5);
  ws.on('message', function incoming(message) {
    logStream.write(message +'\n');
  });
  ws.on('close', function incoming() {
    logStream.destroy();
    clearTimeout(timeout);
  });
});

api.get("/records", async (req, res) => {
  const records = (await fs.readdir("./records")).map((r) => {
    const time = r.replace('.csv', '');
    return {
      id: time
    }
  });
  res.json(records);
})

api.get("/records/:id", async (req, res) => {
  try {
    const content = await fs.readFile(`./records/${req.params.id}.csv`);
    res.end(content);
  } catch (e) {
    console.log(e);
    res.status(404).end('not found');
  }

});

api.listen(8082);
